const { User } = require("../../../models/user");
const auth = require("../../../middleware/auth");
const mongoose = require("mongoose");

describe("auth middleware", () => {
  it("should populate.req.user with the payload of a valid JSON web token", () => {
    const user = { _id: mongoose.Types.ObjectId().toHexString() };
    const token = new User(user).generateAuthToken();
    // Create some mock objects and functions
    const req = { header: jest.fn().mockReturnValue(token) };
    const res = {};
    const next = jest.fn();
    auth(req, res, next);
    expect(req.user).toMatchObject(user);
  });

  it("should return 401 if no token provided in the header", () => {
    let statusCode = 1;
    const req = { header: jest.fn().mockReturnValue("") };
    const res = {
      status: function(code) {
        statusCode = code;
        return {
          send: jest.fn()
        };
      }
    };
    const next = jest.fn();
    auth(req, res, next);
    expect(statusCode).toBe(401);
    expect(req.user).toBeUndefined();
  });

  it("should return 400 if invalid token provided in the header", () => {
    let statusCode = 1;
    const req = { header: jest.fn().mockReturnValue("3939fjofjf9f9f0i99ur9") };
    const res = {
      status: function(code) {
        statusCode = code;
        return {
          send: jest.fn()
        };
      }
    };
    const next = jest.fn();
    auth(req, res, next);
    expect(statusCode).toBe(400);
    expect(req.user).toBeUndefined();
  });
});
