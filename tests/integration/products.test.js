const request = require("supertest");
const { Product } = require("../../models/product");
let server;

describe("/api/products", () => {
  beforeEach(() => {
    server = require("../../index");
  });
  afterEach(async () => {
    server.close();
    // Remove products from test db so that they don't accumulate
    await Product.deleteMany({});
  });

  describe("GET /", () => {
    it("should return all products", async () => {
      await Product.collection.insertMany([
        { name: "Product1" },
        { name: "Product2" }
      ]);

      const res = await request(server).get("/api/products");
      expect(res.status).toBe(200);
      expect(res.body.length).toBe(2);
      expect(res.body.some(g => g.name === "Product1")).toBeTruthy();
      expect(res.body.some(g => g.name === "Product2")).toBeTruthy();
    });
  });

  describe("GET /:id", () => {
    it("should return a product if a valid ID is given", async () => {
      const product = new Product({ name: "My Product", price: 30 });
      await product.save();

      const res = await request(server).get("/api/products/" + product._id);
      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty("name", product.name);
    });

    it("should return 404 if invalid id is given", async () => {
      const res = await request(server).get("/api/products/1");
      expect(res.status).toBe(404);
    });
  });
});
