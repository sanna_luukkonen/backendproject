const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();
const _ = require("lodash");
const { Product, validate, validatePatch } = require("./../models/product");
const auth = require("../middleware/auth");

// Get all products
router.get("/", async (req, res) => {
  const products = await Product.find().sort("name");
  res.send(products);
});

// Get a single product based on id
router.get("/:id", async (req, res) => {
  if (!mongoose.Types.ObjectId.isValid(req.params.id))
    return res.status(404).send("Invalid Object ID given.");

  const product = await Product.findById(req.params.id);
  if (!product)
    return res.status(404).send("The product with the given ID was not found.");
  res.send(product);
});

// Create a new product, access token required
router.post("/", auth, async (req, res) => {
  // Validate input
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  await Product.create(
    _.pick(req.body, [
      "name",
      "description",
      "price",
      "original_price",
      "size",
      "color",
      "image"
    ]),
    (err, prod) => {
      if (err) {
        // Creation fails, send internal server error
        return res.status(500).send(err);
      }
      res.send(prod);
    }
  );
});

// Update existing product, access token required
router.put("/:id", auth, async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Find product to update and update
  const product = await Product.findByIdAndUpdate(
    req.params.id,
    _.pick(req.body, [
      "name",
      "description",
      "price",
      "original_price",
      "size",
      "color",
      "image"
    ]),
    {
      new: true
    }
  );

  if (!product)
    return res.status(404).send("The product with the given ID was not found.");

  res.send(product);
});

// Update only some properties of an existing product
router.patch("/:id", auth, async (req, res) => {
  const { error } = validatePatch(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Find product to update and update
  const product = await Product.findByIdAndUpdate(
    req.params.id,
    { $set: req.body },
    {
      new: true
    }
  );

  if (!product)
    return res.status(404).send("The product with the given ID was not found.");

  res.send(product);
});

// Delete a product
router.delete("/:id", auth, async (req, res) => {
  const product = await Product.findByIdAndRemove(req.params.id);
  if (!product)
    return res.status(404).send("The product with the given ID was not found.");

  res.send(product);
});

module.exports = router;
