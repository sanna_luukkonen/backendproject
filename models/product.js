const mongoose = require("mongoose");
const Joi = require("joi");

const Schema = mongoose.Schema;

const Product = mongoose.model(
  "Product",
  new Schema({
    name: { type: String, required: true },
    description: String,
    price: { type: Number, required: true },
    original_price: Number,
    size: String,
    color: String,
    image: String
  })
);

function validateProduct(product) {
  const schema = {
    name: Joi.string().required(),
    description: Joi.string(),
    price: Joi.number().required(),
    original_price: Joi.number(),
    size: Joi.string(),
    color: Joi.string(),
    image: Joi.string()
  };
  // If original price is smaller than current price, make it the same as current price
  if (product.original_price < product.price) {
    product.original_price = product.price;
  }

  return Joi.validate(product, schema);
}

function validatePatch(product) {
  const schema = {
    name: Joi.string(),
    description: Joi.string(),
    price: Joi.number(),
    original_price: Joi.number(),
    size: Joi.string(),
    color: Joi.string(),
    image: Joi.string()
  };
  return Joi.validate(product, schema);
}

module.exports.Product = Product;
module.exports.validate = validateProduct;
module.exports.validatePatch = validatePatch;
