const express = require("express");
const config = require("config");
const products = require("./routes/products");
const users = require("./routes/users");
const auth = require("./routes/auth");
const bodyparser = require("body-parser");
const mongoose = require("mongoose");
const path = require("path");
const cors = require("cors");

const app = express();

const corsOptions = {
  exposedHeaders: "x-auth-token"
};

app.use(cors(corsOptions));

// View engine
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));

app.use(bodyparser.json());

app.use(express.static(path.join(__dirname, "public")));

// Check that private key for JSON Web Tokens exist, if not, exit the program
if (config.get("jwtPrivateKey") === "") {
  console.error(
    "products_jwtPrivateKey is not defined! Please define it as an environment variable before running the application."
  );
  process.exit(1);
}

// Connect to Mongo DB
const dbConnectionString = config.get("db");
mongoose
  .connect(dbConnectionString, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
  })
  .then(() => console.log(`Connected to ${dbConnectionString}`))
  .catch(err => console.error(`Could not connect to ${dbConnectionString}`));

// Add routes
app.use("/api/products", products);
app.use("/api/users", users);
app.use("/api/auth", auth);

// Start the server
const port = 3000;
const server = app.listen(port, () =>
  console.log(`Listening at localhost, port ${port}`)
);
module.exports = server;
