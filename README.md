This is a product database project with Node.js, Express, REST and MongoDB.
The project contains a React front-end that can be used to view and manipulate products in the database.

To run this project,

1. Download the source code
2. Run npm install
3. Set environment variable products_jwtPrivateKey (e.g. in mac export products_jwtPrivateKey=myKey). Keep your key a secret.
4. Run node index
5. React client will be at http://localhost:3000/.

In the react client you can:
1. View all products in the database (no login required)
2. Registering a new user is done in the Register page. New user is immediately logged in after register.
3. Existing users can login in the Login page.
4. Add a product (login required)
5. Update a product by clicking product name in the product list (login required)
6. Delete a product by clicking a red X in the product list (login required)

